/**
 * 该组件为聊天页面的表情插件的页面布局
 */

/**
 * emo顶部Tabbar的数据结构
 */
class EmoTabData {
  index:number;
  image:Resource;
  tag:string;
  constructor(index:number,image:Resource,tag:string) {
    this.index =index;
    this.image = image;
    this.tag = tag;
  }
}

@Component
export struct emoiLayout {
  private controller : TabsController = new TabsController()
  @State emoTabIndex : number = 1
  private emoItems: EmoData[] = getEmoDatas()
  private emoTabItems: EmoTabData[] = [
    {
      "index" :0,
      "image" : $r('app.media.ic_search'),
      "tag" : "搜索"
    },
    {
      "index" :1,
      "image" : $r('app.media.ic_emoji'),
      "tag" : "表情"
    },
    {
      "index" :3,
      "image" : $r('app.media.ic_love'),
      "tag" : "收藏"
    }]

  /**
   * 顶部标签栏
   */
  @Builder emoTabBar() {
    Row() {
      ForEach(this.emoTabItems,item => {
        Column() {
          Image(item.image)
        }
        .padding(8)
        .height(40)
        .width(40)
        .borderRadius(8)
        .backgroundColor((item.index == this.emoTabIndex) ?
        $r('app.color.white'): $r('app.color.grey4'))
        .onClick(()=> {
          this.controller.changeIndex(item.index)
        })
      },item => item.index.toString())
    }
    .padding({left:10,right: 10})
    .height(50)
    .width('100%')
    .backgroundColor($r('app.color.grey4'))
  }

  /**
   * emo表情标签
   */
  @Builder emoTabContext() {
    Grid() {
      ForEach(this.emoTabItems,item=> {
        GridItem() {
          Image($rawfile(item.file))
            .height(30)
            .width(30)
        }
      },item =>item.tag)
    }
    .columnsTemplate('1fr 1fr 1fr 1fr 1fr 1fr 1fr')
    .width('100%')
    .height(200)
    .rowsGap(10)
    .onScrollIndex((first:number) =>{
    })
    .padding({top:20})
    .backgroundColor($r('app.color.grey0'))
  }

/**
 * 搜索表情标签
 */
  @Builder searchTabContext() {
    Column() {
      TextInput({ placeholder: $r('app.string.emoji_search')})
        .width('90%')
        .height(30)
        .type(InputType.Normal)
        .fontFamily($r('sys.float.ohos_id_text_size_body1'))
        .fontSize(14)
        .placeholderFont({size:15,family:$r('sys.float.ohos_id_text_size_body1')})
        .placeholderColor($r('app.color.grey0'))
        .caretColor($r('app.color.green0'))
        .backgroundColor($r('app.color.grey4'))
        .enterKeyType(EnterKeyType.Search)
      Text($r('app.string.emoji_search_more'))
        .fontSize(15)
        .fontFamily($r('sys.float.ohos_id_text_size_body1'))
        .fontColor($r('app.color.grey3'))
        .margin({ top:10 })
    }.height(200)
    .width('100%')
    .padding({ top:20 })
    .backgroundColor($r('app.color.white'))
  }

  build() {
    Column() {
      /**
       * 顶部导航栏
       */
      this.emoTabBar()
      /**
       * 表情标签页组件
       * 分别为:搜索/系统表情/搜藏
       */
      Tabs({ barPosition:BarPosition.Start,index:1,controller:this.controller }) {
        TabContent() {
          this.searchTabContext()
        }
        TabContent() {
          this.emoTabContext()
        }
        TabContent() {
          Text('结合后端逻辑设计')
            .fontSize(15)
            .fontFamily($r('sys.float.ohos_id_text_size_body1'))
            .fontColor($r('app.color.grey3'))
            .margin({ top: 10 })
        }
      }.onChange((index: number) => {
        this.emoTabIndex = index
      })
      .vertical(false)
      .barHeight(0)
      .width('100%')
      .height(200)
      .scrollable(true)
    }
  }
}